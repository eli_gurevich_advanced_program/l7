#include "sqlite3.h"
#include <iostream>

int callback(void* notUsed, int argc, char** argv, char** azCol);


int main()
{
	int rc = 0;
	sqlite3* db;
	char *zErrMsg = 0;

	
	//try to connct
	rc = sqlite3_open("FirstPart.db", &db);
	
	//in case of failure
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("PAUSE");
		return 1;
	}

	//Create table
	//rc = sqlite3_exec(db, "CREATE TABLE people(id integer PRIMARY KEY AUTOINCREMENT,name VARCHAR )", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)// test if every thing is ok
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	//enter 3 names
	rc = sqlite3_exec(db, "INSERT INTO people (name) VALUES ('Lavi' )", callback, 0, &zErrMsg);

	rc = sqlite3_exec(db, "INSERT INTO people (name) VALUES ('Liroy' )", callback, 0, &zErrMsg);
	
	rc = sqlite3_exec(db, "INSERT INTO people (name) VALUES ('Amit' )", callback, 0, &zErrMsg);

	//update a name 

	rc = sqlite3_exec(db, "update people SET name = 'Eli' where id = 3 ", callback, 0, &zErrMsg);

	
	


	system("PAUSE");
	//close file
	sqlite3_close(db);
}
/*
The function does something
Input: ?
Output: ?
*/
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	return 0;
}



