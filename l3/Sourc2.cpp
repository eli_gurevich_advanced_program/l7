#include "sqlite3.h"
#include <iostream>
#include <string>

#define BALANCE 2
#define PRICE 3
#define AVA 4

int callbackAc(void* notUsed, int argc, char** argv, char** azCol);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
int callbackCar(void* notUsed, int argc, char** argv, char** azCol);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);

int acBalance = 0;
int carPrice = 0;
int ava = 0;
int fromBalance = 0;
int toBalance = 0;

int main()
{
	int rc = 0;
	sqlite3* db;
	char *zErrMsg = 0;


	//try to connct
	rc = sqlite3_open("carsDealer.db", &db);

	//in case of failure
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("PAUSE");
		return 1;
	}
	if (balanceTransfer(1, 2, 16, db, zErrMsg))
	{
		std::cout << "SUCCES!!" << std::endl;
	}
	else
	{
		std::cout << "FAIL!" << std::endl;
	}
	if (carPurchase(1, 2, db, zErrMsg))
	{
		std::cout << "GREAT BUY!!!" << std::endl;
	}
	else
	{
		std::cout << "TOO BAD :C" << std::endl;
	}
	if (carPurchase(1, 3, db, zErrMsg))
	{
		std::cout << "GREAT BUY!!!" << std::endl;
	}
	else
	{
		std::cout << "TOO BAD :C" << std::endl;
	}
	if (carPurchase(1, 4, db, zErrMsg))
	{
		std::cout << "GREAT BUY!!!" << std::endl;
	}
	else
	{
		std::cout << "TOO BAD :C" << std::endl;
	}
	

	system("PAUSE");


}



/*
The function sets shows the balance of an account
Input: None
Output: None
*/
int callbackAc(void* notUsed, int argc, char** argv, char** azCol)
{

	acBalance = atoi(argv[BALANCE]);
	return 0;
}
/*
The function sets shows the balance of an account
Input: None
Output: None
*/
int callbackAcFrom(void* notUsed, int argc, char** argv, char** azCol)
{

	fromBalance = atoi(argv[BALANCE]);
	return 0;
}

/*
The function sets shows the balance of an account
Input: None
Output: None
*/
int callbackAcTo(void* notUsed, int argc, char** argv, char** azCol)
{

	toBalance = atoi(argv[BALANCE]);
	return 0;
}

/*
The function shows the car price and availbility
Input: None
Output: None
*/
int callbackCar(void* notUsed, int argc, char** argv, char** azCol)
{
	carPrice = atoi(argv[PRICE]);
	ava = atoi(argv[AVA]);
	return 0;
}

/*
Nothing
Input: None
Output: None
*/
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	
	return 0;
}




/*
The function checks if an account can buy a car
Input: buyerId, carId, file , string for error
Output: true / false
*/

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	bool cond = false;
	int rc = 0;
	
	std::string com1 = "Select * from accounts where id = " + std::to_string(buyerid);
	std::string com2 = "Select * from cars where id = "+ std::to_string(carid);

	rc = sqlite3_exec(db, com1.c_str(), callbackAc, 0, &zErrMsg);
	rc = sqlite3_exec(db, com2.c_str(), callbackCar, 0, &zErrMsg);

	std::cout << "Price = " << carPrice << std::endl << "Balance = " << acBalance << std::endl;


	

	if (acBalance >= carPrice && ava == 1)
	{
		int left = acBalance - carPrice;
		sqlite3_exec(db, "begin transaction", callback, 0, &zErrMsg);
		sqlite3_exec(db, ("UPDATE cars SET available = 0 WHERE id = " + std::to_string(carid)).c_str(), callback, 0, &zErrMsg);
		sqlite3_exec(db, ("UPDATE accounts SET balance =" + std::to_string(left) + " WHERE id = " + std::to_string(buyerid)).c_str(), callback, 0, &zErrMsg);
		sqlite3_exec(db, "commit", callback, 0, &zErrMsg);
		cond = true;
	}
	return cond;
}

/*
The function transfers money from acc to acc
Input: from , to , amount , file , errMsg
Output: true/false
*/
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc = 0;
	bool cond = false;
	std::string com1 = "Select * from accounts where id = " + std::to_string(from);
	std::string com2 = "Select * from accounts where id = " + std::to_string(to);

	rc = sqlite3_exec(db, com1.c_str(), callbackAcFrom, 0, &zErrMsg);
	rc = sqlite3_exec(db, com2.c_str(), callbackAcTo, 0, &zErrMsg);

	if (amount < fromBalance)
	{
		cond = true;
		sqlite3_exec(db, "begin transaction", callback, 0, &zErrMsg);
		sqlite3_exec(db, ("UPDATE accounts SET balance = " + std::to_string(fromBalance - amount)+ " WHERE id = " + std::to_string(from)).c_str(), callback, 0, &zErrMsg);
		sqlite3_exec(db, ("UPDATE accounts SET balance =" + std::to_string(toBalance + amount) + " WHERE id = " + std::to_string(to)).c_str(), callback, 0, &zErrMsg);
		sqlite3_exec(db, "commit", callback, 0, &zErrMsg);

	}
	return cond;
	
}