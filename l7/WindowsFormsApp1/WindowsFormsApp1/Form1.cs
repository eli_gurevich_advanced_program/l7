﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        bool run = true;
        int YourScore = 0;
        int OpScore = 0;
        bool disable = true;
        string[] names = Directory.GetFiles(@"K:\Magsimim\עקרונות מתקדמים\יא2-רביעי\סמסטר ב\HW7\l7\l7\WindowsFormsApp1\WindowsFormsApp1\Resources", "*.png")
                                     .Select(Path.GetFileName)
                                     .ToArray();

        Random rnd = new Random();
        TcpClient client;
        IPEndPoint serverEndPoint;
        NetworkStream clientStream;
        bool quit = false;
        string enemy = "0000";


        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            int number = rnd.Next(0, 51);
            for (int i = 0; i < names.Length; i++)
            {
                names[i] = names[i].Substring(0, names[i].Length - 4);

                Console.WriteLine(names[i]);

            }
            for (int i = 0; i < names.Length; i++)
            {
                if (names[i].Equals("card_back_red") || names[i].Equals("card_back_blue"))
                {
                    while (names[number].Equals("card_back_red") || names[number].Equals("card_back_blue"))
                    {
                        number = rnd.Next(0, 51);
                    }
                    names[i] = names[number];
                }

            }
            Thread Connection = new Thread(connect);
            Connection.Start();
            while (disable)
            {

                Task.Delay(10).Wait();
            }

            GenerateCards_Click();
            while (quit == false)
            {
                Task.Delay(10).Wait();
            }
            this.Hide();
            this.Close();


        }

        private string Kind(string bytes,string card)
        {
            if(((int)Char.GetNumericValue((bytes.ElementAt(1)))) == 1 && ((int)Char.GetNumericValue((bytes.ElementAt(2)))) != 0)
            {
                if(bytes.ElementAt(4) == 'H')
                {
                    return card + "hearts2";
                }
                if (bytes.ElementAt(4) == 'S')
                {
                    return card + "spades2";
                }
                if (bytes.ElementAt(4) == 'D')
                {
                    return card + "diamonds2";
                }
                if (bytes.ElementAt(4) == 'C')
                {
                    return card + "clubs2";
                }

            }
            else
            {
                if (bytes.ElementAt(4) == 'H')
                {
                    return card + "hearts";
                }
                if (bytes.ElementAt(4) == 'S')
                {
                    return card + "spades";
                }
                if (bytes.ElementAt(4) == 'D')
                {
                    return card + "diamonds";
                }
                if (bytes.ElementAt(4) == 'C')
                {
                    return card + "clubs";
                }
            }
            return "";
        }
        private void send(string bytes)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes(bytes);
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();

        }

        private void connect()
        {
            client = new TcpClient();
            serverEndPoint = new
            IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            client.Connect(serverEndPoint);
            clientStream = client.GetStream();

            byte[] bufferIn = new byte[4];
            int bytesRead = clientStream.Read(bufferIn, 0, 4);
            string input = new ASCIIEncoding().GetString(bufferIn);
            
            MessageBox.Show(input.ElementAt(0).ToString());
            if (input.ElementAt(0) == '0')
            {

                disable = false;
            }
            if (input.ElementAt(0) == '1')
            {
                if (((int)Char.GetNumericValue((input.ElementAt(1)))) == 0)

                {
                    if (((int)Char.GetNumericValue((input.ElementAt(1)))) > 1)
                    {
                        enemy = "_" + (int)Char.GetNumericValue((input.ElementAt(1))) + "_" + "of";

                    }
                    else
                    {
                        enemy = "_" + "ace_of_";
                    }
                }
                else
                {
                    if (((int)Char.GetNumericValue((input.ElementAt(2)))) == 0)
                    {
                        enemy = "_10_of_";
                    }
                    else
                    {
                        if (((int)Char.GetNumericValue((input.ElementAt(2)))) == 1)
                        {
                            enemy = "jack_of_";
                        }
                        else
                        {
                            if (((int)Char.GetNumericValue((input.ElementAt(2)))) == 2)
                            {
                                enemy = "queen_of_";
                            }
                            else
                            {
                                if (((int)Char.GetNumericValue((input.ElementAt(2)))) == 3)
                                {
                                    enemy = "king_of_";
                                }

                            }

                        }

                    }

                }
                enemy = Kind(input, enemy);
                
            }
            if (input.ElementAt(0) == '2')
            {
                quit = true;
            }

        }



        private void GenerateCards_Click()
        {
            Console.Write("\n\n\nEntered Funtiockin\n\n\n\n");

            int amount = 10;
            //red_card_back.Image = Properties.Resources.card_back_red;
            // set the location for where we start drawing the new cards (notice the location+height)
            int x = 120;
            int y = 600;
            Point newLocation = new Point(x, y);
            for (int i = 0; i < amount; i++)
            {
                // create the image object itself
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i; // in our case, this is the only property that changes between the different images
                currentPic.Image = global::WindowsFormsApp1.Properties.Resources.card_back_red;
                currentPic.Location = newLocation;
                currentPic.Size = new System.Drawing.Size(100, 114);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                // assign an event to it

                currentPic.Click += delegate (object sender1, EventArgs e1)
                {



                    int random = rnd.Next(0, 51);

                    object O = Properties.Resources.ResourceManager.GetObject(names[random]);

                    if (O == null)
                    {
                        names[random] = "_" + names[random];
                        O = Properties.Resources.ResourceManager.GetObject(names[random]);




                    };
                    ((PictureBox)sender1).Image = (Image)O;
                    string[] props = names[random].Split('_');
                    int toAdd = 0;
                    toAdd = props[0].ElementAt(0);
                    string toSend = "0000";
                    if (toAdd < 10)
                    {
                        toSend = "0" + toAdd.ToString();
                    }
                    string a = props[2].ElementAt(0).ToString().ToUpper();

                    toSend = "1" + toSend.ToString() + a;
                    send(toSend);

                };

                // add the picture object to the form (otherwise it won't be seen)
                this.Controls.Add(currentPic);

                // calculate the location point for the next card to be drawn/added
                newLocation.X += currentPic.Size.Width + 10;
                if (newLocation.X > this.Size.Width)
                {
                    // move to next line below
                    newLocation.X += currentPic.Width + 20;
                }
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            run = false;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
